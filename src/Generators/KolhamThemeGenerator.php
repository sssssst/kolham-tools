<?php declare(strict_types=1);

namespace Drupal\kolham_tools\Generators;

use DrupalCodeGenerator\Application;
use DrupalCodeGenerator\Command\ThemeGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Implements theme command.
 *
 * @todo: Create a SUT test for this.
 * @todo: Clean-up.
 */
final class KolhamThemeGenerator extends ThemeGenerator {

  protected string $name = 'kolham-theme';
  protected string $alias = 'kolham';
  protected string $description = 'Generates Kolham Base theme';
  protected bool $isNewExtension = TRUE;
  protected string $templatePath = __DIR__ . "/../../templates";



  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $filesystem = new Filesystem();

    $this->collectDefault($vars);

    $vars['base_theme'] = Utils::human2machine($this->ask('Base theme', 'classy'));
    $vars['description'] = $this->ask('Description', 'Kolham subtheme');
    $vars['package'] = $this->ask('Package', 'Custom');
    $vars['config'] = $this->confirm('Would you like to use the config files?', TRUE);


    $this->addFile('{machine_name}/{machine_name}.info.yml', 'starterkit/theme/theme-info.twig');
    $this->addFile('{machine_name}/{machine_name}.libraries.yml', 'starterkit/theme/theme-libraries.twig');
    $this->addFile('{machine_name}/{machine_name}.theme', 'starterkit/theme/theme.twig');

    if($vars['config']) {
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_account_menu.yml', 'starterkit/config/optional/block.block.STARTERKIT_account_menu.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_branding.yml', 'starterkit/config/optional/block.block.STARTERKIT_branding.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_breadcrumbs.yml', 'starterkit/config/optional/block.block.STARTERKIT_breadcrumbs.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_content.yml', 'starterkit/config/optional/block.block.STARTERKIT_content.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_footer.yml', 'starterkit/config/optional/block.block.STARTERKIT_footer.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_help.yml', 'starterkit/config/optional/block.block.STARTERKIT_help.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_local_actions.yml', 'starterkit/config/optional/block.block.STARTERKIT_local_actions.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_local_tasks.yml', 'starterkit/config/optional/block.block.STARTERKIT_local_tasks.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_main_menu.yml', 'starterkit/config/optional/block.block.STARTERKIT_main_menu.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_messages.yml', 'starterkit/config/optional/block.block.STARTERKIT_messages.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_page_title.yml', 'starterkit/config/optional/block.block.STARTERKIT_page_title.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_powered.yml', 'starterkit/config/optional/block.block.STARTERKIT_powered.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_search.yml', 'starterkit/config/optional/block.block.STARTERKIT_search.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_secondary_navigation.yml', 'starterkit/config/optional/block.block.STARTERKIT_secondary_navigation.yml.twig');
      $this->addFile('{machine_name}/config/optional/block.block.{machine_name}_tools.yml', 'starterkit/config/optional/block.block.STARTERKIT_tools.yml.twig');
    }

    try {
      $filesystem->mirror($this->templatePath . "/starterkit/files", 'themes/' . $vars['machine_name']);
      $this->io->writeln('Theme files gekopieerd!');
    } catch (IOExceptionInterface $exception) {
        echo "An error occurred while copying templates directory at ".$exception->getPath();
    }

  }

}
