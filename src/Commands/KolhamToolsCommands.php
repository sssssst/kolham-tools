<?php

namespace Drupal\kolham_tools\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class KolhamToolsCommands extends DrushCommands {

  /**
   * A helpful message.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option array option-name
   *   Description
   * @usage kolham_tools-subTheme foo
   *   Usage description
   *
   * @command kolham_tools:subTheme
   * @aliases kolham
   */
  public function subTheme(array $options = ['option-name' => 'default']) {
    $this->logger()->notice(dt('Use `drush gen kolham` to create a new Kolham subtheme.'));
  }

}