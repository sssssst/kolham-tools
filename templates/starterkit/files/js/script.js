/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function (Drupal, $) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.scripts = {
    attach: function (context, settings) {

      // Remove empty paragraphs
      $('p').each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
      });

      // Messages
      $('.messages .close-message').once().on('click', function() {
        $(this).parent().fadeOut();
      });

      // Remove messages after set time
      // if ($('.messages')[0]) {
      //   var messagesTimeOut;
      //   $('.messages').mouseout( function () {
      //     messagesTimeOut = setTimeout(function() { $('.messages').fadeOut(); }, 10000);
      //   });
      //   $('.messages').mouseover( function () {
      //     clearTimeout(messagesTimeOut);
      //   });
      // }

      // Sticky header
      // $(window).scroll(function() {
      //   var scroll = $(window).scrollTop();
      //   if($(window).width() >= 768) {
      //     if (scroll > 10) {
      //       $('header').addClass('sticky');
      //     } else {
      //       $('header').removeClass('sticky');
      //     }
      //   }
      // }).scroll();

      // $('#hamburger').once().on('click', function() {
      //   var $theBody = $('body');
      //   var $menuOpenClass = 'mobile-menu-opened';
      //   if ($theBody.hasClass($menuOpenClass)) {
      //     $theBody.removeClass($menuOpenClass);
      //   } else {
      //     $theBody.addClass($menuOpenClass);
      //   }
      // });

      // Custom ajax throbber
      // Drupal.theme.ajaxProgressThrobber = function() {
      //   return '<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>';
      // }
    }
  };

  // We pass the parameters of this anonymous function are the global variables
  // that this script depend on. For example, if the above script requires
  // jQuery, you should change (Drupal) to (Drupal, jQuery) in the line below
  // and, in this file's first line of JS, change function (Drupal) to
  // (Drupal, $)
})(Drupal, jQuery);
